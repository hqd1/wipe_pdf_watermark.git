import os
import shutil

import fitz
import numpy as np
from PIL import Image


def split_pdf(file_path, out_path):
    """
    切割pdf为图片
    :param file_path: pdf路径
    :param out_path: 输出图片路径
    :return: 输出路径
    """
    pdf = fitz.open(file_path)
    count = 0
    print("##### 开始保存切割图片 #####")
    for page in pdf:
        image_list = page.get_images()
        for img_info in image_list:
            pix = fitz.Pixmap(pdf, img_info[0])
            pix.save(os.path.join(out_path, '%d.jpg' % count))
            count += 1
    print("##### 保存切割图片完毕 #####")
    print("##### {0} 包含 {1} 张图片 #####".format(file_path, count))
    return out_path


def get_image_arr(img):
    """
    获取图片三色数组
    :param img:图片
    :return: 图片编码、三色数组
    """
    img_arr = np.asarray(img, dtype=np.double)
    # 分离通道
    r_img = img_arr[:, :, 0].copy()
    g_img = img_arr[:, :, 1].copy()
    b_img = img_arr[:, :, 2].copy()
    img = r_img * 256 * 256 + g_img * 256 + b_img
    return img, r_img, g_img, b_img


def replace_clr_color(img, src_clr, dst_clr):
    """
    通过矩阵操作颜色替换程序
    @param  img:    图像矩阵
    @param  src_clr:    需要替换的颜色(r,g,b)
    @param  dst_clr:    目标颜色        (r,g,b)
    @return             替换后的图像矩阵
    """

    img, r_img, g_img, b_img = get_image_arr(img)
    src_color = src_clr[0] * 256 * 256 + src_clr[1] * 256 + src_clr[2]

    # 索引并替换颜色
    r_img[img == src_color] = dst_clr[0]
    g_img[img == src_color] = dst_clr[1]
    b_img[img == src_color] = dst_clr[2]

    return compound_img(r_img, g_img, b_img)


def compound_img(r_img, g_img, b_img):
    """
    合并图片
    :param r_img: 红色
    :param g_img: 绿色
    :param b_img: 蓝色
    :return: 图片
    """
    # 合并通道
    dst_img = np.array([r_img, g_img, b_img], dtype=np.uint8)
    # 将数据转换为图像数据(h,w,c)
    dst_img = dst_img.transpose(1, 2, 0)
    return dst_img


def replace_pure_color(img, src_color, dst_color):
    """
    通过矩阵操作颜色替换程序（纯色）
    :param img: 图像矩阵
    :param src_color: 需要替换的颜色
    :param dst_color: 目标颜色
    :return: 图片
    """

    img, r_img, g_img, b_img = get_image_arr(img)
    src_color = src_color * 256 * 256 + src_color * 256 + src_color
    # 索引并替换颜色
    r_img[img >= src_color] = dst_color
    g_img[img >= src_color] = dst_color
    b_img[img >= src_color] = dst_color

    return compound_img(r_img, g_img, b_img)


def list_file(path, suffix=None):
    """
    获取指定目录下指定后缀文件
    :param path: 路径
    :param suffix: 后缀名
    :return: 文件集合
    """
    file_names = os.listdir(path);
    # 获取文件名
    if suffix is not None:
        file_names = [file_name for file_name in file_names if file_name.endswith(suffix)]
    file_names.sort(key=lambda x: int(x[:(-len(suffix))]))
    # 文件名拼接路径
    return [os.path.join(path, file) for file in file_names]


def wipe_watermark(img_file, start_color):
    """
    去除图片水印
    :param start_color: 颜色起始替换位置
    :param img_file: 图片文件
    :return:
    """

    img = replace_pure_color(Image.open(img_file).convert('RGB'), start_color, 255)
    res_img = Image.fromarray(img)
    res_img.save(img_file)


def save_pdf(src_pdf, dest_pdf, file_list):
    """
    生成最终pdf文件
    :param src_pdf: 源文件
    :param dest_pdf: 目标文件
    :param file_list: 图片列表
    :return:
    """
    pdf = fitz.open(src_pdf)
    index = 0
    try:
        for page in pdf:
            # 去除超链接
            for link in page.get_links():
                page.delete_link(link)
            # 替换图片
            for img in page.get_images():
                page._insert_image(filename=file_list[index], _imgname=img[7])
                index = index + 1
        pdf.save(dest_pdf)
    finally:
        pdf.close()


def start(file_path, dest_path, start_color, out_path=r'out'):
    if os.path.exists(out_path):
       # shutil.rmtree(out_path)
        raise FileExistsError('文件夹:{0} 已存在'.format(out_path))
    os.mkdir(out_path)

    print("####### 开始切割pdf：{0} #######".format(file_path))
    split_pdf(file_path, out_path)
    print("####### 切割pdf完毕：{0} #######".format(file_path))
    # 获取文件名
    file_list = list_file(out_path, ".jpg")

    print("####### 开始去除水印 #######")
    for img_file in file_list:
        wipe_watermark(img_file, start_color)
    print("####### 去除水印结束 #######")

    # 生成pdf
    print("####### 生成pdf文件：{0} #######".format(dest_path))
    save_pdf(src_pdf=file_path, dest_pdf=dest_path, file_list=file_list)
    print("####### 生成pdf文件：{0} 完成 #######".format(dest_path))
    # make_pdf(dest_path, file_list)


if __name__ == '__main__':
    # replace_pdf(src_pdf="深入剖析TOMCAT.pdf", dest_pdf='a.pdf', file_list=list_file('out', ".jpg"))
    start(file_path="深入剖析TOMCAT.pdf", dest_path="a.pdf", start_color=230)
    # www.TopSage.com
    # start(file_path="重构-改善既有代码的设计.pdf", dest_path="b.pdf", start_color=175)
